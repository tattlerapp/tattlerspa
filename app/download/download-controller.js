app.controller("DownloadController", function($scope, $http){
	
	$scope.save = function(){
		
		var route = $scope.$root.server;

		var request = {
			FullName: $scope.request.fullName,
			Email: $scope.request.email
		};

		$http.post(route, request);			
	}
});