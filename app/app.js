var app = angular.module("tattlerApp", ["ngRoute", "routeStyles",'chieffancypants.loadingBar', 'ngAnimate'])
    .config(function($routeProvider){
        $routeProvider
            .when("/", {
                templateUrl: "home/home.html",                
                css: ["components/animate/animate.css", "home/home.css"]                
            })
            .when("/download", {
                templateUrl: "contact/contact.html",
                css: "contact/contact.css",
                controller: "ContactController"               
            })
            .when("/contact", {
                templateUrl: "contact/contact.html",
                css: "contact/contact.css",
                controller: "ContactController"
            })
            .when("/business", {
                templateUrl: "business/business.html",
                css: "business/business.css",
                controller: "BusinessController"                
            })
            .otherwise({
                redirectTo:"/"
            });
    })
    .directive("viewContainer", function(){
        return {                         
            link: function postLink(scope, element, attrs){               
                scope.$on("$routeChangeSuccess", function(){
                    $(window).scrollTop(0);
                });

                scope.$on("$viewContentLoaded", function(){
                    $(".carousel").carousel();
                });             
            }
        };    
    });
    
app.run(function($rootScope){
});

$(document).ready(function () {
    onChangeSection($("#menu .navbar-nav > li > a[href='" + window.location.hash + "']"));
    onScrollScreen();
    onClickSectionLink();    
});

function onChangeSection(link) {    
    $("#menu .navbar-nav > li").removeClass("active");
    link.parent().addClass("active");
}

function onScrollScreen(){
    $(window).scroll(function(){ 
        if($(window).scrollTop() > 0)
        {            
            $("#menu").addClass("opaque");
            return;            
        }
        
        $("#menu").removeClass("opaque");
    });
}

function onClickSectionLink(){
    $("#menu .navbar-nav > li > a").click(function(){
        if(!$("#menuToggler").hasClass("collapsed")){
            $("#menuToggler").click();
        }

        onChangeSection($(this));        
    });    
}