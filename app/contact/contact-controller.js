app.controller("ContactController", function($scope, $http){
	
  $scope.originalContact = angular.copy($scope.newEmail);
  var route = $scope.$root.server + "sendemail";

  $scope.resetContactForm = function(isOk){
    $scope.successfull = isOk;
    $scope.showMessage = true;
    $scope.newEmail = angular.copy($scope.originalUser);
    $scope.contactForm.$setPristine();
    $scope.submitProgress = false;
    $scope.showValidation = false;
  }

  $scope.sendEmail = function(userDetails){
		if(!$scope.contactForm.$invalid){	
      $scope.submitProgress = true;
			$http.post(route,userDetails).
          success(function(data,status) {
            $scope.resetContactForm(true);
        	}).
          error(function(data,status){
            $scope.resetContactForm(false);
          });
		} else{
			$scope.showValidation = true;
		}
	}

	$scope.getError = function (error) {
		if(angular.isDefined(error)){
			if(error.required){
				return "Por favor, introduzca un valor.";
			} else if(error.email){
				return "Por favor, introduzca un Email v\u00e1lido";
			} else if(error.pattern){
				return "Por favor, introduzca un valor v\u00e1lido";
			} else if(error.maxlength){
				return "Sobrepaso el l\u00edmite de caracteres permitido";
			} 
		}
	}
  $scope.getMessage = function (isOk) {
    if(angular.isDefined(isOk)){
      if(isOk){
        return "<strong>Gracias por contactarnos,</strong> en breve nos comunicaremos con usted.";
      }  
      else {
        return "Lo sentimos mucho, en estos momentos no podemos atenderlo, por favor vuelva a intentarlo m&#225;s tarde.";
      } 
    }
  }
});