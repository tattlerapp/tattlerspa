app.directive("displayRow", function(){
	return {
		link: function postLink(scope, element, attrs){
			var image = $(element).find("#titleDisplay img");

			var setBackground = function(){	
				$(element).height($(window).height());	
			};

			var setPhoneImage = function(){
				if($(window).height() <= 480){
					$(image).attr("src", "components/app-data/img/home/phone-xs.png");				
					return;
				}				

				$(image).attr("src", "components/app-data/img/home/phone-sm.png");
			};			

			$(window).on("resize", function(){	
				setBackground();
				setPhoneImage();				
			});

			setBackground();
			setPhoneImage();
		}
	};
})
.directive("middleRow", function(){
	return{
		link: function postLink(scope, element, attrs){
			var setHeight = function(){			
				var rows = $(element).find("div > div.row");
				$(rows).removeAttr("style");

				if($(window).width() >= 992)
				{					
					var heights = $(rows).map(function(){
						return $(this).height();
					}).get();

					$(rows).css({"min-height" : Math.max.apply(null, heights) + "px"});
				}
			};

			$(window).on("resize", function(){				
				setHeight();			
			});		

			setHeight();
		}
	};
});