app.controller("BusinessController", function($scope, $http){

	$scope.save = function(){
		var route = $scope.$root.server + "companies/register";

		var request = {
			Name: $scope.request.company,
			RFC: $scope.request.rfc,
			PhoneNumber: $scope.request.phoneNumber
		};

		$http.post(route, request)
			.success(function(data, status){
				$scope.status = status;
				$scope.data = data;
			})
			.error(function(data, status){
				$scope.status = status;
				$scope.data = data || "Request Failed";
			});
	};
});