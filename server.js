var express = require('express');
var nodemailer = require('nodemailer');
var bodyParser = require('body-parser');
var app = express();
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.all('*', function(req,res,next){
  res.header("Access-Control-Allow-origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'tattler.application@gmail.com',
        pass: 'rwyzevgtdbytrlvg'
    }
});

//app.use(express.static(__dirname + '/app')); //Comentar esta linea para solo usar el node para backend


app.post('/sendemail',jsonParser, cors(), function(req, res) { 
  var mailOptions = {
    from: 'Soporte Tattler<tattler.application@gmail.com>', 
	  to: 'contacto@tattler.mx, raulliraguzman@gmail.com', 
	  subject: req.body.contactSubject,
	  html: req.body.contactDesc +'<br/><br/><b>Name:</b>' + req.body.contactName + '<br/><b>Email:</b>' +req.body.contactEmail+'<br/><b>Numero de Contacto:</b>'+ req.body.contactNumber
  };
 
  transporter.sendMail(mailOptions, function(error, response){
      if(error){
          console.log(error);
          res.status(404).send('Sorry, we cannot find that!');
      }else{
          res.status(202).send('Message successfully sent to tattler Mobile S.A.');
      }
  });
});
 

app.listen(8080);
console.log("App listening on port 8080");